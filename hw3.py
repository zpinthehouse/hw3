#---------Imports
from numpy import arange, sin, pi
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
import Tkinter as tk
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import double_pendulum as dp
#---------End of imports

class Foo:
    def __init__(self):
        self._bar = 'hello'

    @property
    def bar(self):
        return self._bar

    @bar.setter
    def bar(self, new):
        self._bar = new

class Application(tk.Frame):

    def say_hi(self):
        print "hi there, everyone!"

    def createWidgets(self):    
        self.add_button_panel(self).pack({"side":"left"})
        #self.add_button_panel_P(self).pack({"side":"left"})
        self.add_animation_canvas(self).pack({"side":"left"})
        FigureCanvasTkAgg(self.sim_fig, master=self).get_tk_widget().pack()
        self.zoomed(self).pack({"side":"right"})
        #self.fig.show()
    


    def re_simulate(self):
        #self.add_button_panel(self).pack_forget()
        #self.add_button_panel(self).destroy()
        self.sim.G=self.G_slider.get()
        self._N = self.N_slider.get()
        #self.add_button_panel(self).pack({"side":"left"})
        self.sim.L[0]=self.L1_slider.get()
        self.sim.L[1]=self.L2_slider.get()
        self.sim.L[2]=self.L3_slider.get()
        self.sim.L[3]=self.L4_slider.get()

        self.sim.M[0]=self.M1_slider.get()
        self.sim.M[1]=self.M2_slider.get()
        self.sim.M[2]=self.M3_slider.get()
        self.sim.M[3]=self.M4_slider.get()

        self.sim.simulate(self._N)
        #self.sim.begin_animation(5)
        self.theta_1_line.set_ydata(self.sim.get_data()[:,0])
        self.theta_2_line.set_ydata(self.sim.get_data()[:,2])
        self.theta_3_line.set_ydata(self.sim.get_data()[:,4])
        self.theta_4_line.set_ydata(self.sim.get_data()[:,6])

        self.theta_dot_1_line.set_ydata(self.sim.get_data()[:,self.sim.get_data_indexes()["point 1 x"]])
        self.theta_dot_2_line.set_ydata(self.sim.get_data()[:,self.sim.get_data_indexes()["point 2 x"]])
        self.theta_dot_3_line.set_ydata(self.sim.get_data()[:,self.sim.get_data_indexes()["point 3 x"]])
        self.theta_dot_4_line.set_ydata(self.sim.get_data()[:,self.sim.get_data_indexes()["point 4 x"]])

        self.theta_1_ax.relim()
        self.theta_2_ax.relim()
        self.theta_3_ax.relim()
        self.theta_4_ax.relim()

        self.theta_dot_1_ax.relim()
        self.theta_dot_2_ax.relim()
        self.theta_dot_3_ax.relim()
        self.theta_dot_4_ax.relim()


    def add_button_panel(self,parent):
        button_pannel=tk.Frame(master=self)
        self.add_quit_button(button_pannel).pack({"side": "bottom"})
        self.add_Listbox(button_pannel).pack()
        #self.add_choice_button(button_pannel).pack({"side": "bottom"})
        self.add_hello_button(button_pannel).pack({"side": "bottom"})
        self.add_resimulate_button(button_pannel).pack()
        self.add_G_slider(button_pannel).pack()
        self.add_N_slider(button_pannel).pack()
        #self._N = self.N_slider.get()

        
        self.add_L1_slider(button_pannel).pack()
        self.add_M1_slider(button_pannel).pack()

        self.add_L2_slider(button_pannel).pack()
        self.add_M2_slider(button_pannel).pack()

        self.add_L3_slider(button_pannel).pack()
        self.add_M3_slider(button_pannel).pack()

        self.add_L4_slider(button_pannel).pack()
        self.add_M4_slider(button_pannel).pack()

        return button_pannel

    def curselect(self,evt):
        value=self._lb.get(self._lb.curselection())
        return value

    def add_Listbox(self,parent):
        frame=tk.Frame(parent)

        self._lb=tk.Listbox(frame)
        
        self._lb.insert("end","theta1")
        self._lb.insert("end","theta2")
        self._lb.insert("end","theta3")
        self._lb.insert("end","theta4") 
        self._lb.insert("end","X1")
        self._lb.insert("end","X2")
        self._lb.insert("end","X3")
        self._lb.insert("end","X4") 
        self._lb.bind("<Double-Button-1>", self.new_pic)
        # self.s =  self._lb.bind("<<ListboxSelect>>",self.curselet)
        # self.new_pic(self.s)
        self._lb.pack()
        return frame

    def zoomed(self,parent):
        fig1=plt.figure(figsize=(8,10))
        self.xx = np.arange(0, 2*np.pi, 0.01)        # x-array
        self.canvas_1 = FigureCanvasTkAgg(fig1, master=self)
        self.theta_10_ax = fig1.add_subplot(111)
        self.theta_10_line, = self.theta_10_ax.plot(self.sim.get_data()[:,10], self.sim.get_data()[:,0])
        self.theta_10_dot, = self.theta_10_ax.plot(0,0,'ro')
        self.theta_10_ax.set_xlabel("$t$")
        self.num_data10=int((self.sim.Tf-self.sim.T0)/self.sim.dt)
        self.ani10 = animation.FuncAnimation(fig1, self.animate10, np.arange(0, self.num_data10), interval=50, blit=False)
        return self.canvas_1.get_tk_widget()

    def animate10(self,i):
        t=i*float(1.0/self.num_data10)*(self.sim.Tf-self.sim.T0)
        
        #self.theta_1_line.set_ydata(np.sin(self.x+t))  # update the datas

        self.theta_10_dot.set_data(t,self.sim.get_data()[i,self.ii])


  
        #self.theta_1_dot.set_ydata()
        return self.theta_10_line

    def new_pic(self,evt):
        widget = evt.widget
        selection=widget.curselection()
        self.value = widget.get(selection[0])
        if self.value== "theta1":
            
            self.theta_10_line.set_ydata(self.sim.get_data()[:,0])
            self.theta_10_ax.relim()
            self.ii = 0

        if self.value=="theta2":
            
            self.theta_10_line.set_ydata(self.sim.get_data()[:,2])
            self.theta_10_ax.relim()
            self.ii = 2

        if self.value=="theta3":
            self.theta_10_line.set_ydata(self.sim.get_data()[:,4])
            self.theta_10_ax.relim()

            self.ii = 4
        if self.value=="theta4":
            self.theta_10_line.set_ydata(self.sim.get_data()[:,6])
            self.theta_10_ax.relim()
            self.ii = 6

        if self.value=="X1":
            self.theta_10_line.set_ydata(self.sim.get_data()[:,11])
            self.theta_10_ax.relim()
            self.ii = 11
        if self.value=="X2":
            self.theta_10_line.set_ydata(self.sim.get_data()[:,12])
            self.theta_10_ax.relim()
            self.ii = 12

        if self.value=="X3":
            self.theta_10_line.set_ydata(self.sim.get_data()[:,13])
            self.theta_10_ax.relim()
            self.ii = 13

        if self.value=="X4":
            self.theta_10_line.set_ydata(self.sim.get_data()[:,14])
            self.theta_10_ax.relim()
            self.ii = 14

        
   
    def add_G_slider(self,parent):
        frame=tk.Frame(parent)
        text=tk.Label(frame)
        text["text"]="gravity"
        text.pack({"side":"left"})
        self.G_slider = tk.Scale(frame, from_=-9.8, to=9.8, orient=tk.HORIZONTAL, resolution=0.01)
        self.G_slider.pack()
        self.G_slider.set(9.8)
        return frame

    def add_N_slider(self,parent):
        frame=tk.Frame(parent)
        text=tk.Label(frame)
        text["text"]="number of pendulums"
        text.pack({"side":"left"})
        self.N_slider = tk.Scale(frame, from_=1, to=4, orient=tk.HORIZONTAL, resolution=1)
        self.N_slider.pack()
        self.N_slider.set(2)
        return frame

    def add_L1_slider(self,parent):
        frame=tk.Frame(parent)
        text=tk.Label(frame)
        text["text"]="length 1"
        text.pack({"side":"left"})
        self.L1_slider = tk.Scale(frame, from_=0.5, to=3.0, orient=tk.HORIZONTAL, resolution=0.01)
        self.L1_slider.pack()
        self.L1_slider.set(1.0)
        return frame

    def add_L2_slider(self,parent):
        frame=tk.Frame(parent)
        text=tk.Label(frame)
        text["text"]="length 2"
        text.pack({"side":"left"})
        self.L2_slider = tk.Scale(frame, from_=0.5, to=3.0, orient=tk.HORIZONTAL, resolution=0.01)
        self.L2_slider.pack()
        self.L2_slider.set(1.0)
        return frame

    def add_L3_slider(self,parent):
        frame=tk.Frame(parent)
        text=tk.Label(frame)
        text["text"]="length 3"
        text.pack({"side":"left"})
        self.L3_slider = tk.Scale(frame, from_=0.5, to=3.0, orient=tk.HORIZONTAL, resolution=0.01)
        self.L3_slider.pack()
        self.L3_slider.set(1.0)
        return frame

    def add_L4_slider(self,parent):
        frame=tk.Frame(parent)
        text=tk.Label(frame)
        text["text"]="length 4"
        text.pack({"side":"left"})
        self.L4_slider = tk.Scale(frame, from_=0.5, to=3.0, orient=tk.HORIZONTAL, resolution=0.01)
        self.L4_slider.pack()
        self.L4_slider.set(1.0)
        return frame



    def add_M1_slider(self,parent):
        frame=tk.Frame(parent)
        text=tk.Label(frame)
        text["text"]="mass 1"
        text.pack({"side":"left"})
        self.M1_slider = tk.Scale(frame, from_=0.5, to=3.0, orient=tk.HORIZONTAL, resolution=0.01)
        self.M1_slider.pack()
        self.M1_slider.set(1.0)
        return frame

    def add_M2_slider(self,parent):
        frame=tk.Frame(parent)
        text=tk.Label(frame)
        text["text"]="mass 2"
        text.pack({"side":"left"})
        self.M2_slider = tk.Scale(frame, from_=0.5, to=3.0, orient=tk.HORIZONTAL, resolution=0.01)
        self.M2_slider.pack()
        self.M2_slider.set(1.0)
        return frame

    def add_M3_slider(self,parent):
        frame=tk.Frame(parent)
        text=tk.Label(frame)
        text["text"]="mass 3"
        text.pack({"side":"left"})
        self.M3_slider = tk.Scale(frame, from_=0.5, to=3.0, orient=tk.HORIZONTAL, resolution=0.01)
        self.M3_slider.pack()
        self.M3_slider.set(1.0)
        return frame

    def add_M4_slider(self,parent):
        frame=tk.Frame(parent)
        text=tk.Label(frame)
        text["text"]="mass 4"
        text.pack({"side":"left"})
        self.M4_slider = tk.Scale(frame, from_=0.5, to=3.0, orient=tk.HORIZONTAL, resolution=0.01)
        self.M4_slider.pack()
        self.M4_slider.set(1.0)
        return frame



    def add_quit_button(self,parent):
        quit_button = tk.Button(parent)
        quit_button["text"] = "QUIT"
        quit_button["fg"]   = "red"
        quit_button["command"] =  self.quit
        return quit_button

    def add_hello_button(self,parent):
        hi_there = tk.Button(parent)
        hi_there["text"] = "Hello",
        hi_there["command"] = self.say_hi
        return hi_there

    def add_resimulate_button(self,parent):
        resim = tk.Button(parent)
        resim["text"] = "Re-Simulate",
        resim["command"] = self.re_simulate
        return resim

    # def run_new_simulation(self, parameters):
    #     self.data=
    
    def add_animation_canvas(self,parent):
        fig = plt.Figure(figsize=(10,40))
        self.x = np.arange(0, 2*np.pi, 0.01)        # x-array
        self.canvas = FigureCanvasTkAgg(fig, master=self)

        self.theta_1_ax = fig.add_subplot(8,1,1)
        self.theta_1_line, = self.theta_1_ax.plot(self.sim.get_data()[:,10], self.sim.get_data()[:,0])
        self.theta_1_dot, = self.theta_1_ax.plot(0,0,'ro')
        self.theta_1_ax.set_xlabel("$t$")
        self.theta_1_ax.set_ylabel(r"$\theta_{1}$")
        
        self.theta_2_ax = fig.add_subplot(8,1,2)
        self.theta_2_line, = self.theta_2_ax.plot(self.sim.get_data()[:,10], self.sim.get_data()[:,2])
        self.theta_2_dot, = self.theta_2_ax.plot(0,0,'ro')
        self.theta_2_ax.set_xlabel("$t$")
        self.theta_2_ax.set_ylabel(r"$\theta_{2}$")

        self.theta_3_ax = fig.add_subplot(8,1,3)
        self.theta_3_line, = self.theta_3_ax.plot(self.sim.get_data()[:,10], self.sim.get_data()[:,4])
        self.theta_3_dot, = self.theta_3_ax.plot(0,0,'ro')
        self.theta_3_ax.set_xlabel("$t$")
        self.theta_3_ax.set_ylabel(r"$\theta_{3}$")

        self.theta_4_ax = fig.add_subplot(8,1,4)
        self.theta_4_line, = self.theta_4_ax.plot(self.sim.get_data()[:,10], self.sim.get_data()[:,6])
        self.theta_4_dot, = self.theta_4_ax.plot(0,0,'ro')
        self.theta_4_ax.set_xlabel("$t$")
        self.theta_4_ax.set_ylabel(r"$\theta_{4}$")

        self.theta_dot_1_ax = fig.add_subplot(10,1,6)
        self.theta_dot_1_line, = self.theta_dot_1_ax.plot(self.sim.get_data()[:,10], self.sim.get_data()[:,self.sim.get_data_indexes()["point 1 x"]])
        self.theta_dot_1_dot, = self.theta_dot_1_ax.plot(0,0,'ro')
        self.theta_dot_1_ax.set_xlabel("$t$")
        self.theta_dot_1_ax.set_ylabel(r"$P_{1x}$")
        
        self.theta_dot_2_ax = fig.add_subplot(10,1,7)
        self.theta_dot_2_line, = self.theta_dot_2_ax.plot(self.sim.get_data()[:,10], self.sim.get_data()[:,self.sim.get_data_indexes()["point 2 x"]])
        self.theta_dot_2_dot, = self.theta_dot_2_ax.plot(0,0,'ro')
        self.theta_dot_2_ax.set_xlabel("$t$")
        self.theta_dot_2_ax.set_ylabel(r"$P_{2x}$")

        self.theta_dot_3_ax = fig.add_subplot(10,1,8)
        self.theta_dot_3_line, = self.theta_dot_3_ax.plot(self.sim.get_data()[:,10], self.sim.get_data()[:,self.sim.get_data_indexes()["point 3 x"]])
        self.theta_dot_3_dot, = self.theta_dot_3_ax.plot(0,0,'ro')
        self.theta_dot_3_ax.set_xlabel("$t$")
        self.theta_dot_3_ax.set_ylabel(r"$P_{3x}$")

        self.theta_dot_4_ax = fig.add_subplot(10,1,9)
        self.theta_dot_4_line, = self.theta_dot_4_ax.plot(self.sim.get_data()[:,10], self.sim.get_data()[:,self.sim.get_data_indexes()["point 4 x"]])
        self.theta_dot_4_dot, = self.theta_dot_4_ax.plot(0,0,'ro')
        self.theta_dot_4_ax.set_xlabel("$t$")
        self.theta_dot_4_ax.set_ylabel(r"$P_{4x}$")

        self.num_data=int((self.sim.Tf-self.sim.T0)/self.sim.dt)
        self.ani = animation.FuncAnimation(fig, self.animate, np.arange(0, self.num_data), interval=50, blit=False)
        return self.canvas.get_tk_widget()


    def animate(self,i):
        t=i*float(1.0/self.num_data)*(self.sim.Tf-self.sim.T0)
        
        #self.theta_1_line.set_ydata(np.sin(self.x+t))  # update the datas
        self.theta_1_dot.set_data(t,self.sim.get_data()[i,0])
        self.theta_2_dot.set_data(t,self.sim.get_data()[i,2])
        self.theta_3_dot.set_data(t,self.sim.get_data()[i,4])
        self.theta_4_dot.set_data(t,self.sim.get_data()[i,6])
 
        self.theta_dot_1_dot.set_data(t,self.sim.get_data()[i,self.sim.get_data_indexes()["point 1 x"]])
        self.theta_dot_2_dot.set_data(t,self.sim.get_data()[i,self.sim.get_data_indexes()["point 2 x"]])
        self.theta_dot_3_dot.set_data(t,self.sim.get_data()[i,self.sim.get_data_indexes()["point 3 x"]])
        self.theta_dot_4_dot.set_data(t,self.sim.get_data()[i,self.sim.get_data_indexes()["point 4 x"]])
  
        #self.theta_1_dot.set_ydata()
        return self.theta_1_line, self.theta_2_line, self.theta_3_line, self.theta_4_line
    def __init__(self, master=None):
        tk.Frame.__init__(self, master)
        self._N = 2
        self.ii = 0
        self.pack()
        self.sim = dp.Double_Pendulum_Simulation()
        self.sim_fig=self.sim.generate_figure()
        self.sim.init()
        self.sim.simulate(2)
        self.createWidgets()
        self.sim.begin_animation()


def main():
    root = tk.Tk()
    app = Application(master=root)
    app.mainloop()
    root.destroy()
    

if __name__ == "__main__":
    main()

