# Double pendulum formula translated from the C code at
# http://www.physics.usyd.edu.au/~wheat/dpend_html/solve_dpend.c
# @editor: gray_thomas

from numpy import sin, cos, pi, array
import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as integrate
import matplotlib.animation as animation

class Double_Pendulum_Simulation:
    def __init__(self):
        self.G =  9.8 # acceleration due to gravity, in m/s^2
        self.L=[1.0,1.0,1.0,1.0]
        self.M=[1.0,1.0,1.0,1.0]
        self.T0 = 0 # seconds
        self.Tf = 20 # seconds
        self.dt = 0.1 # seconds
        self.theta=[30.0,-10.0,20.0,-30.0]
        self.w=[0.0,0.0,0.0,0.0]
        self.state=[30.0*pi/180.0,0.0*pi/180.0,-10.0*pi/180.0,0.0*pi/180.0,20.0*pi/180.0,0.0*pi/180.0,-30.0*pi/180.0,0.0*pi/180.0]
        # self.init()
# find the Lagrange equation
    def derivs_1(self,state,t):
        g=self.G
        l1=self.L[0]
        m1=self.M[0]
        dydx = np.zeros_like(state)
        th1 = state[0]
        dydx[0] = state[1]
        dydx[1] = -g/l1*sin(state[0])
        dydx[2] = 1
        return dydx

    def derivs_2(self, state, t):
        G=self.G
        L1=self.L[0]
        L2=self.L[1]
        M1=self.M[0]
        M2=self.M[1]
        dydx = np.zeros_like(state)
        dydx[0] = state[1]

        del_ = state[2]-state[0]
        den1 = (M1+M2)*L1 - M2*L1*cos(del_)*cos(del_)
        dydx[1] = (M2*L1*state[1]*state[1]*sin(del_)*cos(del_)
                 + M2*G*sin(state[2])*cos(del_) + M2*L2*state[3]*state[3]*sin(del_)
                 - (M1+M2)*G*sin(state[0]))/den1

        dydx[2] = state[3]

        den2 = (L2/L1)*den1
        dydx[3] = (-M2*L2*state[3]*state[3]*sin(del_)*cos(del_)
                 + (M1+M2)*G*sin(state[0])*cos(del_)
                 - (M1+M2)*L1*state[1]*state[1]*sin(del_)
                 - (M1+M2)*G*sin(state[2]))/den2
        dydx[4]=1
        return dydx

    def derivs_3(self, state, t):
        g=self.G
        l1=self.L[0]
        l2=self.L[1]
        l3=self.L[2]
        m1=self.M[0]
        m2=self.M[1]
        m3=self.M[2]
        dydx = np.zeros_like(state)
        th1 = state[0]
        dth1=state[1]
        th2 = state[2]
        dth2=state[3]
        th3 = state[4]
        dth3=state[5]

        a1 = l1*(m1+m2+m3)  # dv1 parameter
        a2 = m2*l2*cos(th1 - th2) + m3*l2*cos(th1 - th2)# dv2 paramter
        a3 = l3*m3*cos(th1 - th3)
        a4 = (m2*l2*sin(th1 - th2) + l2*m3*sin(th1 - th2))*dth2*dth2 + dth3**2*l3*m3*sin(th1 - th3) + (m1 + m2 + m3)*g*sin(th1)
        #eq of th2
        b1 = m2*l1*cos(th1 - th2) + l1*m3*cos(th1 - th2) # dv1 parameter
        b2 = m2*l2 + l2*m3# dv2 parameter
        b3 = l3 * m3 *cos(th2 - th3) 
        b4 = (-l1*sin(th1-th2)*m2-l1*m3*sin(th1 - th2))*dth1*dth1 + l3*m3*sin(th2 - th3)*dth3**2+(m2 + m3)*g*sin(th2)

        c1 = l1*cos(th1 - th3)
        c2 = l2*cos(th2 - th3)
        c3 = l3
        c4 = -dth1**2*l1*sin(th1 - th3) - dth2**2*l2*sin(th2 - th3) + g*sin(th3)
        dv1, dv2,dv3= np.linalg.solve([[a1,a2,a3],[b1,b2,b3],[c1,c2,c3]], [-a4,-b4,-c4])
        dydx[0]= state[1]
        dydx[1] = dv1
        dydx[2] = state[3]
        dydx[3] = dv2
        dydx[4] = state[5]
        dydx[5] = dv3
        dydx[6] = 1
        return dydx

    def derivs_4(self, state, t):
        g=self.G
        l1=self.L[0]
        l2=self.L[1]
        l3=self.L[2]
        l4=self.L[3]
        m1=self.M[0]
        m2=self.M[1]
        m3=self.M[2]
        m4=self.M[3]
        dydx = np.zeros_like(state)
        th1 = state[0]
        dth1=state[1]
        th2 = state[2]
        dth2=state[3]
        th3 = state[4]
        dth3=state[5]
        th4 = state[6]
        dth4 = state[7]

        a1 = l1*(m1+m2+m3+m4)  # dv1 parameter
        a2 = m2*l2*cos(th1 - th2) + m3*l2*cos(th1 - th2)+ l2*m4*cos(th1-th2)# dv2 paramter
        a3 = l3*m3*cos(th1 - th3)+l3*m4*cos(th1 - th3)
        a4 = l4 * m4 *cos(th1 - th4)
        a5 = (m2*l2*sin(th1 - th2) + l2*m3*sin(th1 - th2)+ l2*m4*sin(th1 - th2))*dth2*dth2 + dth3**2*(l3*m3*sin(th1 - th3) +l3*m4*sin(th1 - th3))+ dth4**2*l4*m4*sin(th1 - th4) + (m1 + m2 + m3 + m4)*g*sin(th1)

        #eq of th2
        b1 = m2*l1*cos(th1 - th2) + l1*m3*cos(th1 - th2) + l1*m4*cos(th1 - th2) # dv1 parameter
        b2 = m2*l2 + l2*m3 + l2*m4# dv2 parameter
        b3 = l3 * m3 *cos(th2 - th3) + l3* m4*cos(th2 - th3)
        b4 = l4*m4*cos(th2 - th4) 
        b5 = (-l1*sin(th1-th2)*m2 - l1*m3*sin(th1 - th2) - l1*m4*sin(th1 - th2))*dth1*dth1 + (l3*m3*sin(th2 - th3)+l3*m4*sin(th2 - th3))*dth3**2 + dth4**2*l4*m4*sin(th2 - th4) + (m2 + m3 + m4)*g*sin(th2)

        c1 = l1*m3*cos(th1 - th3) + l1*m4*cos(th1 - th3)
        c2 = l2*m3*cos(th2 - th3) +l2*m4*cos(th2 - th3)
        c3 = l3*m3 + l3*m4
        c4 = l4*m4*cos(th3 - th4)
        c5 = dth1**2*(-l1**m3*sin(th1 - th3) - l1*m4*sin(th1 - th3)) + dth2**2*(-l2*m3*sin(th2 - th3) - l2*m4*sin(th2 - th3)) + dth4**2*l4*m4*sin(th3 - th4) + (m3+m4)*g*sin(th3)

        d1 = l1 * cos(th1 - th4) 
        d2 = l2 * cos(th2 - th4)
        d3 = l3 * cos(th3 - th4)
        d4 = l4
        d5 = -dth1**2*l1*sin(th1 - th4) - dth2**2*l2*sin(th2 - th4) - dth3**2*l3*sin(th3 - th4)+g*sin(th4)
        dv1, dv2,dv3 ,dv4= np.linalg.solve([[a1,a2,a3,a4],[b1,b2,b3,b4],[c1,c2,c3,c4],[d1,d2,d3,d4]], [-a5,-b5,-c5,-d5])
        dydx[0]= state[1]
        dydx[1] = dv1
        dydx[2] = state[3]
        dydx[3] = dv2
        dydx[4] = state[5]
        dydx[5] = dv3
        dydx[6] = state[7]
        dydx[7] = dv4
        dydx[8] = 1
        return dydx

    # def derivs_5(self, state, t):
    #     g=self.G
    #     l1=self.L[0]
    #     l2=self.L[1]
    #     l3=self.L[2]
    #     l4=self.L[3]
    #     l5=self.L[4]
    #     m1=self.M[0]
    #     m2=self.M[1]
    #     m3=self.M[2]
    #     m4=self.M[3]
    #     m5=self.M[4]
    #     dydx = np.zeros_like(state)
    #     th1 = state[0]
    #     th2 = state[2]
    #     th3 = state[4]
    #     th4 = state[6]
    #     th5 = state[8]
    #     dth1=state[1]
    #     dth2=state[3]
    #     dth3=state[5]
    #     dth4=state[7]
    #     dth5=state[9]
    #     a1 = l1*(m1+m2+m3+m4+m5)  # dv1 parameter
    #     a2 = m2*l2*cos(th1 - th2) + m3*l2*cos(th1 - th2)+ l2*m4*cos(th1-th2) + m5*l2*cos(th1 - th2)# dv2 paramter
    #     a3 = l3*m3*cos(th1 - th3)+l3*m4*cos(th1 - th3)+l3*m5*cos(th1 - th3)
    #     a4 = l4 * m4 *cos(th1 - th4) + l4 * m5 * cos(th1 - th4)
    #     a5 = m5 * l5 * cos(th1 - th5)
    #     a6 = (m2*l2*sin(th1 - th2) + l2*m3*sin(th1 - th2)+ l2*m4*sin(th1 - th2) + l2*m5*sin(th1 - th2))*dth2*dth2 + dth3**2*(l3*m3*sin(th1 - th3) +l3*m4*sin(th1 - th3)+l3*m5*sin(th1 - th3))+ dth4**2*(l4*m4*sin(th1 - th4)+l4*m5*sin(th1 - th4)) +dth5**2*l5*m5*sin(th1 - th5) + (m1 + m2 + m3 + m4 + m5)*g*sin(th1)

    #     #eq of th2
    #     b1 = m2*l1*cos(th1 - th2) + l1*m3*cos(th1 - th2) + l1*m4*cos(th1 - th2) + m5*l1*cos(th1 - th2) # dv1 parameter
    #     b2 = m2*l2 + l2*m3 + l2*m4 + l2*m5# dv2 parameter
    #     b3 = l3 * m3 *cos(th2 - th3) + l3* m4*cos(th2 - th3) + l3*m5*cos(th2 - th3)
    #     b4 = l4*m4*cos(th2 - th4) + l4* m5*cos(th2 - th4)
    #     b5 = l5*m5*cos(th2 - th5)
    #     b6 = (-l1*sin(th1-th2)*m2 - l1*m3*sin(th1 - th2) - l1*m4*sin(th1 - th2) - l1*m5*sin(th1 - th2))*dth1*dth1 + (l3*m3*sin(th2 - th3)+l3*m4*sin(th2 - th3) + l3*m5*sin(th2 - th3))*dth3**2 + dth4**2*(l4*m4*sin(th2 - th4) +l4*m5*sin(th2 - th4)+ dth5**2*l5*m5*sin(th2 - th5)+(m2 + m3 + m4 + m5)*g*sin(th2))

    #     c1 = l1*m3*cos(th1 - th3) + l1*m4*cos(th1 - th3) + l1*m5*cos(th1 - th3)
    #     c2 = l2*m3*cos(th2 - th3) +l2*m4*cos(th2 - th3) + l2*m5*cos(th2 - th3)
    #     c3 = l3*m3 + l3*m4 + l3*m5
    #     c4 = l4*m4*cos(th3 - th4) + l4*m5*cos(th3 - th4)
    #     c5 = l5*m5*cos(th3 - th5)
    #     c6 = dth1**2*(-l1**m3*sin(th1 - th3) - l1*m4*sin(th1 - th3) - l1*m5*sin(th1 - th3)) + dth2**2*(-l2*m3*sin(th2 - th3) - l2*m4*sin(th2 - th3) - l2*m5*sin(th2 - th3)) + dth4**2*(l4*m4*sin(th3 - th4) + l4*m5*sin(th3 - th4)) + dth5**2*l5*m5*sin(th3 - th5) + (m3+m4+m5)*g*sin(th3)

    #     d1 = m4*l1 * cos(th1 - th4) +l1*m5*cos(th1 - th4)
    #     d2 = l2 *m4* cos(th2 - th4) + l2*m5*cos(th2 - th4)
    #     d3 = l3 * m4*cos(th3 - th4) + l3*m5*cos(th3 - th4)
    #     d4 = l4*m4 + l4*m5
    #     d5 = l5*m5*cos(th4 - th5)
    #     d6 = dth1**2*l1*(-m4*sin(th1 - th4) - m5*sin(th1 - th4)) + dth2**2*l2*(-m4*sin(th2 - th4) -m5*sin(th2 - th4)) +  dth3**2*l3*(-m4*sin(th3 - th4) - m5*sin(th3 - th4)) + dth5**2*l5*m5*sin(th4 - th5) + (m4 + m5)*g*sin(th4)

    #     e1 = l1*cos(th1 - th5)
    #     e2 = l2*cos(th2 - th5)
    #     e3 = l3*cos(th3 - th5)
    #     e4 = l4*cos(th4 - th5)
    #     e5 = l5*m5
    #     e6 = -dth1**2*l1*sin(th1 - th5) - dth2**2*l2*sin(th2 - th5) - dth3**2*l3*sin(th3 - th5) - dth4**2*l4*sin(th4 - th5) + g*sin(th5)
    #     dv1,dv2,dv3,dv4,dv5= np.linalg.solve([[a1,a2,a3,a4,a5],[b1,b2,b3,b4,b5],[c1,c2,c3,c4,c5],[d1,d2,d3,d4,d5],[e1,e2,e3,e4,e5]], [-a6,-b6,-c6,-d6,-e6])
    #     dydx[0]= state[1]
    #     dydx[1] = dv1
    #     dydx[2] = state[3]
    #     dydx[3] = dv2
    #     dydx[4] = state[5]
    #     dydx[5] = dv3
    #     dydx[6] = state[7]
    #     dydx[7] = dv4
    #     dydx[8] = state[9]
    #     dydx[9] = dv5
    #     dydx[10] = 1
    #     return dydx

    def simulate(self,n): 
        G=self.G
        L=self.L[0:n]
        M=self.M[0:n]
        # create a time array from 0..100 sampled at 0.1 second steps
        
        self.t = np.arange(self.T0, self.Tf, self.dt)

        theta=self.theta[0:n]
        w=self.w[0:n]

        self.rad = pi/180

        state=self.state[0:2*n]
        state.append(self.t[0])
        print state
        # integrate your ODE using scipy.integrate.
        

        if n==1:

            self.y = integrate.odeint(self.derivs_1, state, self.t)


            self.x1 = L[0]*sin(self.y[:,[0]])
            self.y1 = -L[0]*cos(self.y[:,[0]])
            self.x2 = np.zeros_like(self.x1)
            self.y2 = np.zeros_like(self.y1)
            self.x3 = np.zeros_like(self.x1)
            self.y3 = np.zeros_like(self.y1)
            self.x4 = np.zeros_like(self.x1)
            self.y4 = np.zeros_like(self.y1)
            self.x5 = np.zeros_like(self.x1)
            self.y5 = np.zeros_like(self.y1)
            self.y0 = np.zeros_like(self.x1)
            self.data = np.concatenate((self.y[:,0:2],self.y0,self.y0,self.y0,self.y0,self.y0,self.y0,self.y0,self.y0,self.y[:,[2]],self.x1,self.x2,self.x3,self.x4,self.x5,self.y1,self.y2,self.y3,self.y4,self.y5),axis=1)
            self.data_indexes={"theta 1":0,"theta dot 1":5,"theta 2":1,"theta dot 2":6,"theta 3":2,"theta dot 3":7,"theta 4":3,"theta dot 4":8,"theta 5":4,"theta dot 5":9,"time":10,"point 1 x":11,"point 2 x":12,"point 3 x":13,"point 4 x":14,"point 5 x":15,"point 1 y":16, "point 2 y":17,"point 3 y":18,"point 4 y":19,"point 5 y":20}

            
        if n==2:
            self.y = integrate.odeint(self.derivs_2, state, self.t)
            self.x1 = L[0]*sin(self.y[:,[0]])
            self.y1 = -L[0]*cos(self.y[:,[0]])
            self.x2 = L[1]*sin(self.y[:,[2]]) + self.x1
            self.y2 = -L[1]*cos(self.y[:,[2]]) + self.y1
            self.x3 = np.zeros_like(self.x1)
            self.y3 = np.zeros_like(self.y1)
            self.x4 = np.zeros_like(self.x1)
            self.y4 = np.zeros_like(self.y1)
            self.x5 = np.zeros_like(self.x1)
            self.y5 = np.zeros_like(self.y1)
            self.y0 = np.zeros_like(self.x1)
            self.data = np.concatenate((self.y[:,0:4],self.y0,self.y0,self.y0,self.y0,self.y0,self.y0,self.y[:,[4]],self.x1,self.x2,self.x3,self.x4,self.x5,self.y1,self.y2,self.y3,self.y4,self.y5),axis=1)
            self.data_indexes={"theta 1":0,"theta dot 1":5,"theta 2":1,"theta dot 2":6,"theta 3":2,"theta dot 3":7,"theta 4":3,"theta dot 4":8,"theta 5":4,"theta dot 5":9,"time":10,"point 1 x":11,"point 2 x":12,"point 3 x":13,"point 4 x":14,"point 5 x":15,"point 1 y":16, "point 2 y":17,"point 3 y":18,"point 4 y":19,"point 5 y":20}

        if n==3:
            self.y = integrate.odeint(self.derivs_3, state, self.t)
            self.x1 = L[0]*sin(self.y[:,[0]])
            self.y1 = -L[0]*cos(self.y[:,[0]])
            self.x2 = L[1]*sin(self.y[:,[2]]) + self.x1
            self.y2 = -L[1]*cos(self.y[:,[2]]) + self.y1
            self.x3 = L[2]*sin(self.y[:,[4]])+ self.x2
            self.y3 = -L[2]*cos(self.y[:,[4]]) + self.y2
            self.x4 = np.zeros_like(self.x1)
            self.y4 = np.zeros_like(self.y1)
            self.x5 = np.zeros_like(self.x1)
            self.y5 = np.zeros_like(self.y1)
            self.y0 = np.zeros_like(self.x1)
            self.data = np.concatenate((self.y[:,0:6],self.y0,self.y0,self.y0,self.y0,self.y[:,[6]],self.x1,self.x2,self.x3,self.x4,self.x5,self.y1,self.y2,self.y3,self.y4,self.y5),axis=1)
            self.data_indexes={"theta 1":0,"theta dot 1":5,"theta 2":1,"theta dot 2":6,"theta 3":2,"theta dot 3":7,"theta 4":3,"theta dot 4":8,"theta 5":4,"theta dot 5":9,"time":10,"point 1 x":11,"point 2 x":12,"point 3 x":13,"point 4 x":14,"point 5 x":15,"point 1 y":16, "point 2 y":17,"point 3 y":18,"point 4 y":19,"point 5 y":20}

        if n==4:
            self.y = integrate.odeint(self.derivs_4, state, self.t)
            self.x1 = L[0]*sin(self.y[:,[0]])
            self.y1 = -L[0]*cos(self.y[:,[0]])
            self.x2 = L[1]*sin(self.y[:,[2]]) + self.x1
            self.y2 = -L[1]*cos(self.y[:,[2]]) + self.y1
            self.x3 = L[2]*sin(self.y[:,[4]])+ self.x2
            self.y3 = -L[2]*cos(self.y[:,[4]]) + self.y2
            self.x4 = L[3]*sin(self.y[:,[6]])+ self.x3
            self.y4 = -L[3]*cos(self.y[:,[6]]) + self.y3
            self.x5 = np.zeros_like(self.y1)
            self.y5 = np.zeros_like(self.y1)
            self.y0 = np.zeros_like(self.x1)
            self.data = np.concatenate((self.y[:,0:8],self.y0,self.y0,self.y[:,[8]],self.x1,self.x2,self.x3,self.x4,self.x5,self.y1,self.y2,self.y3,self.y4,self.y5),axis=1)
            self.data_indexes={"theta 1":0,"theta dot 1":5,"theta 2":1,"theta dot 2":6,"theta 3":2,"theta dot 3":7,"theta 4":3,"theta dot 4":8,"theta 5":4,"theta dot 5":9,"time":10,"point 1 x":11,"point 2 x":12,"point 3 x":13,"point 4 x":14,"point 5 x":15,"point 1 y":16, "point 2 y":17,"point 3 y":18,"point 4 y":19,"point 5 y":20}

 

    
    def get_data(self):
        return self.data
    def get_data_indexes(self):
        return self.data_indexes
# draw the pendulums
    def init(self):
        self.line, = self.ax.plot([], [], 'o-', lw=2)
        self.time_template = 'time = %.1fs'
        self.time_text = self.ax.text(0.05, 0.9, '', transform=self.ax.transAxes)
        self.line.set_data([], [])
        self.time_text.set_text('')
        return self.line, self.time_text

    def init_animation(self):
        self.line.set_data([], [])
        self.time_text.set_text('')
        return self.line, self.time_text


    def animate(self,i):
        self.thisx = [0, self.x1[i], self.x2[i], self.x3[i], self.x4[i], self.x5[i]]
        self.thisy = [0, self.y1[i], self.y2[i], self.y3[i], self.y4[i], self.y5[i]]

        self.line.set_data(self.thisx, self.thisy)
        self.time_text.set_text(self.time_template%(i*self.dt))
        return self.line, self.time_text


    def begin_animation(self):

        self.ani = animation.FuncAnimation(self.fig, self.animate, np.arange(1, len(self.y)),interval=50, blit=True, init_func=self.init_animation)
        
    def generate_figure(self):
        self.fig = plt.figure()
        self.ax = self.fig.add_subplot(111, autoscale_on=False, xlim=(-5, 5), ylim=(-5, 5))
        self.ax.grid()
        return self.fig

#ani.save('double_pendulum.mp4', fps=15, clear_temp=True)

# def main():
#     sim = Pendulum_Simulation()
#     sim.generate_figure()
#     sim.init()
#     sim.simulate()
#     sim.G=-9.8
#     sim.M1=2
#     sim.simulate()
#     sim.begin_animation()
#     plt.show()

# if __name__ == "__main__":
#     main()
